/*-- List.cpp------------------------------------------------------------
 
   This file implements List member functions.
  
-------------------------------------------------------------------------*/

#include <cassert>
using namespace std;

#include "list.h"

//--- Definition of class constructor
List::List()                 
: mySize(0)
{}

//--- Definition of empty()
bool List::empty() const
{
   return mySize == 0;
}

//--- Definition of display()
void List::display(ostream & out) const
{
   for (int i = 0; i < mySize; i++)
     out << myArray[i] << "  ";
}

//--- Definition of output operator
ostream & operator<< (ostream & out, const List & aList)
{
   aList.display(out);
   return out;
}

//--- Definition of insert()
void List::insert(ElementType item, int pos)
{
   if (mySize == CAPACITY)
   {
      cerr << "*** No space for list element -- terminating "
              "execution ***\n";
      exit(1);
   }
   if (pos < 0 || pos > mySize)
   {
      cerr << "*** Illegal location to insert -- " << pos 
           << ".  List unchanged. ***\n";
      return;
   }

   // First shift array elements right to make room for item

   for(int i = mySize; i > pos; i--)
      myArray[i] = myArray[i - 1];

   // Now insert item at position pos and increase list size  
   myArray[pos] = item;
   mySize++;
}

//--- Definition of erase()
void List::erase(int pos)
{
   if (mySize == 0)
   {
      cerr << "*** List is empty ***\n";
      return;
   }
   if (pos < 0 || pos >= mySize)
   {
      cerr << "Illegal location to delete -- " << pos
           << ".  List unchanged. ***\n";
      return;
   }

   // Shift array elements left to close the gap
   for(int i = pos; i < mySize; i++)
       myArray[i] = myArray[i + 1];

   // Decrease list size
    mySize--;
}



void List::push(ElementType e)  {
  insert(e,mySize);
}


void List::sort() {
  // Step through each element of the array
  for (int nStartIndex = 0; nStartIndex < mySize; nStartIndex++)
  {
      // nSmallestIndex is the index of the smallest element
      // we've encountered so far.
      int nSmallestIndex = nStartIndex;
   
      // Search through every element starting at nStartIndex+1
      for (int nCurrentIndex = nStartIndex + 1; nCurrentIndex < mySize; nCurrentIndex++)
      {
          // If the current element is smaller than our previously found smallest
          if (myArray[nCurrentIndex] < myArray[nSmallestIndex])
              // Store the index in nSmallestIndex
              nSmallestIndex = nCurrentIndex;
      }
   
      // Swap our start element with our smallest element
      swap(myArray[nStartIndex], myArray[nSmallestIndex]);
  }
}

bool List::operator==(const List& L) const {
  if (mySize != L.mySize) return false;
  for (int i=0; i<mySize; i++) {
    //cout << myArray[i] << " " << L.myArray[i] << endl;
    if (myArray[i] != L.myArray[i]) return false;
  }
  return true;
}

//Function that counts how many times a number is found in the list
int List::count(ElementType e) const{
	int count=0; 
	
	//GOes thru all elements n the array
	for (int i = 0; i < mySize; i++)
		//If the current number is the same, adds one to the counter 
		if ( myArray[i] == e)
			count++;
	return count; //Returns the counter
}

//Function that returns a number and erases it from the list.
ElementType List::pop(int pos){
	ElementType a; 	  //Will return the number
	a = myArray[pos]; //Holds the number on the called position
	erase(pos);       //Deletes the number form the list
	
return a; //Returns the number
}

//Does the same as the former pop, but on the last position by default
ElementType List::pop(){
	return pop(mySize);
}

//Merges two lists
List List::merge(List & L){
	int i = 0, j = 0;
	List L3; //Will hold the list to return
	
	//Goes tru the lists 
	for (; i < mySize && j < (L.mySize-1); i++, j++) {
		//If the number is smaller then the one on the other list
		//It adds it in the position on the third list and subtracts
		//the position on the second list
		if (myArray[i] <= L.myArray[j]){
			L3.push(myArray[i]);
			j--;
		}
		
		//Else, it adds the second list's number and subtracts the 
		//position on the first list.
		else {
			L3.push(L.myArray[j]);
			i--;
		}
	}
	
	List Q;
	int minindex;
	
	//Depending on the condtion, it continues to add the numbers 
	//from a respectve list to a fourth list
	if (i < mySize)
		for (int p=i; p < mySize; p++)
			Q.push(myArray[p]);
	if (j < L.mySize)
		for (int p=j; p < L.mySize; p++)
                        Q.push(L.myArray[p]);

	//Adds the fourth list to the third list
	if (i < mySize || j < L.mySize){
		minindex = 0;
		for (int a=1; a < Q.mySize; a++){
			if (Q.myArray[a] < Q.myArray[minindex])
				minindex = a;
			if ((a+1)==Q.mySize){
				L3.push(Q.myArray[minindex]);
				Q.erase(minindex);
				a=-1;
				minindex = 0;
			}
		}
	}

	return L3;  //Returns the third list
}

